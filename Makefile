PREFIX := /usr

all:

bindir:
	install -d $(DESTDIR)$(PREFIX)/bin

install: | bindir
	install -m 755 scripts/filter_log.sh $(DESTDIR)$(PREFIX)/bin/filter_log
	install -m 755 scripts/get_python_deps.sh $(DESTDIR)$(PREFIX)/bin/get_python_deps
	install -m 755 scripts/gitlab_upload_package.sh $(DESTDIR)$(PREFIX)/bin/gitlab_upload_package
	install -m 755 scripts/incus_helper.sh $(DESTDIR)$(PREFIX)/bin/incus_helper
	install -m 755 scripts/parona_update.sh $(DESTDIR)$(PREFIX)/bin/parona_update

perl: | bindir
	install -m 755 scripts/gitlab_artifact_cleanup.pl $(DESTDIR)$(PREFIX)/bin/gitlab_artifact_cleanup

efibootmgr:
	install -d $(DESTDIR)/etc/kernel/postinst.d/
	install -m 755 installkernel-hooks/postinst.d/92-efibootmgr.sh $(DESTDIR)/etc/kernel/postinst.d/92-efibootmgr.install

.PHONY: all bindir install perl efibootmgr
