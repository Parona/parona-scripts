# Scripts and hooks that I use on my machines

Pre-commit is used for linting and static analysis.
```bash
emerge -n dev-util/sh dev-util/shellcheck dev-vcs/pre-commit
pre-commit install # in the repo
```

Anything here is licensed GPL-3
