#!/usr/bin/env bash

if [[ $1 == "-p" ]]; then
  pretend=1
fi

efibootmgr() {
  if [[ -z ${pretend} ]]; then
    command efibootmgr "$@"
  else
    echo "efibootmgr $*"
  fi
}

boot_fstab=$(awk '$2 == "/boot" { print 1; exit }' /etc/fstab)
boot_proc=$(awk '$2 == "/boot" { print $1; exit }' /proc/mounts)

if [[ -n ${boot_fstab} ]] && [[ -z ${boot_proc} ]]; then
  echo "/boot not mounted"
  exit 1
fi

root_fstab=$(awk '$2 == "/" { print $1; exit }' /etc/fstab)
root_proc=$(awk '$2 == "/" { print $1; exit }' /proc/mounts)

root_blkid=$(blkid | awk "\$1 == \"${root_proc}:\" { gsub(\"\\\"\",\"\"); print}")

if [[ -z "${root_blkid}" ]]; then
  echo "Root couldn't be parsed from blkid"
  echo "Most likely not enough permissions and wasn't cached previously"
  exit 1
fi

if ! [[ "${root_blkid}" =~ ${root_fstab} ]]; then
  echo "fstab and mounted / dont match"
  exit 1
fi

extra_args="root=$(echo "${root_blkid}" | awk '{if ($6) {print $6} else {print $2}}') $(cat /etc/kernel/extra-kernel-args 2>/dev/null)"

# Remove old options
for line in $(command efibootmgr); do
  if [[ ${line} =~ ^Boot([0-9]{4}) ]]; then
    efibootmgr -q -B -b "${BASH_REMATCH[1]}"
  fi
done

# Discover and add kernels
kernels_added=0

for version in $(versionsort /boot/vmlinuz-* 2>/dev/null | tac); do
  if [[ -e /boot/initramfs-${version}.img ]]; then
    initrd="initrd=initramfs-${version}.img "
  fi
  efibootmgr -q -c -L "${version}" -l "vmlinuz-${version}" \
    -d "$(lsblk --list --noheadings --paths --output PKNAME "${boot_proc}")" \
    -u "${initrd}${extra_args}"
  unset initrd
  ((kernels_added += 1))
done

if [[ ${kernels_added} == 0 ]]; then
  echo "No kernels added, something is wrong"
  exit 1
fi

efibootmgr -q -o $(seq -s , 0 $((kernels_added - 1)))

command efibootmgr -u
