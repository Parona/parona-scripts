#!/usr/bin/env bash
set -e

type=$(file -b --mime-type "${1}")

extract=()
case ${type} in
  application/gzip)
    extract=(gzip -dc "${1}")
    ;;
  application/x-xz)
    extract=(xz -dc "${1}")
    ;;
  *)
    echo "${type} not handled"
    exit 1
    ;;
esac

out="$(basename "${1}")"
out="${out%.*}"
out="${out}.xz"

directory="${2-${PWD}}"

"${extract[@]}" | ansifilter | xz -9 >"${directory}/${out}"
