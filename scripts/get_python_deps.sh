#!/usr/bin/env bash

# Script to parse python module dependencies
# Copyright (C) 2022-2024 Alfred Wingate
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# TODO:
# * better argument parsing

set -e

if [[ -z $1 || -z $2 || $# -gt 2 ]]; then
  echo "Arguments are packagename and searchpath"
  exit 1
fi

if [[ -z ${EPYTHON} ]]; then
  EPYTHON=python
fi

if ! type -P "${EPYTHON}" >/dev/null || ! [[ "$("${EPYTHON}" --version)" =~ ^Python ]]; then
  echo "EPYTHON ${EPYTHON} not valid"
  exit 1
fi

packagename=$1
searchpath=$2

if ! [[ -d "${searchpath}" ]]; then
  echo "${searchpath} is not a directory"
  exit 1
fi

# shellcheck disable=SC2207
MODULES=(
  $(
    grep -EIhr \
      "(^|^\s*)(import [[:alnum:]_.]*|from [[:alnum:]_.]* import ([[:alnum:]_.]*|\*))($|,| as)" \
      "${searchpath}" |
      sed -E \
        -e 's/^\s*//' \
        -e 's/^from ([[:alnum:]_\.]*) .*/\1/' \
        -e '/import [[:alnum:]_.]*(\s*)?,/! s/import ([[:alnum:]_.]*).*/\1/' \
        -e '/import/ {
            y/,/\n/
            s/import//
            s/[ \t]*//g
        }' |
      sort |
      uniq |
      sed -E "/^${packagename}(\.|$)/d"
  )
)

for module in "${MODULES[@]}"; do
  spec=$(${EPYTHON} -c "import ${module}; print(${module}.__spec__)" 2>/dev/null) ||
    {
      echo "Module ${module} not installed"
      continue
    }

  # Ignore built in modules
  if [[ ${spec} =~ "origin='built-in'" ]]; then
    continue
  fi
  path=$(${EPYTHON} -c "import ${module}; print(${module}.__file__)" 2>/dev/null) ||
    {
      echo "!!! ${module} has __spec__ attribute and isnt built-in but doesnt have __file__"
      continue
    }
  package=$(qfile -q "${path}" 2>/dev/null) ||
    {
      echo "Module ${module} found at path ${path}, but it isn't managed by portage"
      continue
    }
  if [[ "${package}" != "dev-lang/python" ]]; then
    echo "${package} from ${module}"
  fi
done
