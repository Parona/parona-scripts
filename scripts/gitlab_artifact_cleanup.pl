#!/usr/bin/env perl
use autodie;
use strict;
use warnings;

use feature qw(try);
no warnings qw(experimental::try);

use DateTime::Format::ISO8601;
use Getopt::Long;
use GitLab::API::v4;
use Number::Bytes::Human qw(format_bytes);

my $v4_api_url = 'https://gitlab.com/api/v4/';
my $token      = undef;
my $project_id = undef;
my $before     = undef;

GetOptions(
    'v4_api_url=s' => \$v4_api_url,
    'token=s'      => \$token,
    'project_id=i' => \$project_id,
    'before=s'     => \$before
) or exit 1;

if ( !defined $token ) {
    print "token must be supplied with --token\n";
    exit 1;
}

if ( !defined $project_id ) {
    print "project_id must be supplied with --project_id\n";
    exit 1;
}

my $before_date = DateTime->now;

if ( defined $before ) {
    my $duration = undef;

    if ( $before =~ /([[:digit:]]*)d/sxm ) {
        $duration = DateTime::Duration->new( days => $1 );
    }
    elsif ( $before =~ /([[:digit:]]*)w/sxm ) {
        $duration = DateTime::Duration->new( weeks => $1 );
    }
    elsif ( $before =~ /([[:digit:]]*)y/sxm ) {
        $duration = DateTime::Duration->new( years => $1 );
    }
    else {
        print
            "--before requires a wholenumber followed by d/w/y identifier for day, week or year\n";
        exit 1;
    }
    $before_date->subtract_duration($duration);
}

my $api = GitLab::API::v4->new(
    url           => $v4_api_url,
    private_token => $token,
);

my $jobs_json = $api->paginator( 'jobs', $project_id )->all();

my @cleanup = ();

foreach ( @{$jobs_json} ) {
    my %job_hash  = %{$_};
    my $id        = ${job_hash}{'id'};
    my @artifacts = @{ ${job_hash}{'artifacts'} };
    my $date      = DateTime::Format::ISO8601->parse_datetime(
        ${job_hash}{'finished_at'} );

    if ( DateTime->compare( $before_date, $date ) == 1
        && scalar @artifacts > 0 )
    {
        push @cleanup, \%job_hash;
    }
}

if ( scalar @cleanup == 0 ) {
    print "No artifacts to be deleted\n";
    exit;
}

my $filesize = 0;
print "Artifacts to be deleted:\n";
foreach my $job (@cleanup) {
    print "${$job}{'id'} ${$job}{'finished_at'}:";
    foreach my $artifact ( @{ ${$job}{'artifacts'} } ) {
        print " ${$artifact}{'filename'}";
        $filesize += ${$artifact}{'size'};
    }
    print "\n";
}

print "Total file size to be deleted: ${\(format_bytes($filesize))}\n\n";

print "Do you want to proceed? (y/n)\n";
while (1) {
    my $answer = <>;
    if ( $answer =~ /[yY]/sxm ) {
        last;
    }
    elsif ( $answer =~ /[nN]/sxm ) {
        exit;
    }
    else {
        print "Didn't understand\n";
    }
}

foreach my $job (@cleanup) {
    try {
        $api->_call_rest_client(
            'POST',
            'projects/:project_id/jobs/:job_id/erase',
            [ ( $project_id, ${$job}{'id'} ) ], {},
        );
    }
    catch ($e) {
        print "$e\n";
        exit 1;
    }
}
