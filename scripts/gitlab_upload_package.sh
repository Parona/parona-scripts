#!/usr/bin/env bash
set -e

pretend=
privatetoken=
onlyvalidate=

usage() {
  echo "Usage: $0 [-h|-p|-v] [ -t token ] projectid packagename packageversion file"
}

OPTS="$(getopt --name="${0}" --options='hpt:v' -- "${@}")"
eval set -- "${OPTS}"
while true; do
  case "${1}" in
    -h)
      usage
      exit 0
      ;;
    -p)
      pretend="1"
      shift
      ;;
    -t)
      privatetoken="${2}"
      shift 2
      ;;
    -v)
      onlyvalidate=1
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      usage
      exit 2
      ;;
  esac
done

validate() {
  # Sanity check token. Not revoken, reasonable scope etc. User access to repo isnt checked.
  json_output="$(curl -s --header "PRIVATE-TOKEN: ${privatetoken}" "https://gitlab.com/api/v4/personal_access_tokens/self")"
  if ! echo "${json_output}" | jq -e '.message != "401 Unauthorized"' >/dev/null; then
    echo "Token isn't valid"
    exit 1
  fi
  if ! echo "${json_output}" | jq -e 'all(. ; [.revoked == false, .active == true, any(.scopes[] ; . == "api")])' >/dev/null; then
    echo "Token isn't expired, revoked or lacking required scope"
    exit 1
  fi
}

if [[ -z ${privatetoken} ]] && [[ -e ~/.gitlab_token ]]; then
  privatetoken=$(cat ~/.gitlab_token)
else
  echo "No private token found"
  exit 1
fi

if [[ -n ${onlyvalidate} ]]; then
  validate
  exit 0
fi

projectid="${1}"
if [[ -z ${projectid} ]]; then
  usage
  exit 2
fi
packagename="${2}"
if [[ -z ${packagename} ]]; then
  usage
  exit 2
fi
packageversion="${3}"
if [[ -z ${packageversion} ]]; then
  usage
  exit 2
fi
file="${4}"
if [[ -z ${file} ]]; then
  usage
  exit 2
fi

if ! [[ -e ${file} ]]; then
  echo "No file ${file}"
  exit 1
fi

validate

set -- curl \
  --progress-bar \
  --upload-file "${file}" \
  --header "PRIVATE-TOKEN: ${privatetoken}" \
  "https://gitlab.com/api/v4/projects/${projectid}/packages/generic/${packagename}/${packageversion}/$(basename "${file}")"

if [[ -z ${pretend} ]]; then
  output="$("${@}")"
  if echo "${output}" | jq -e '.message == "201 Created"' >/dev/null; then
    echo "Successfully uploaded package"
    exit 0
  else
    echo "Failed to upload package:"
    echo "${output}"
    exit 1
  fi
else
  echo "$@"
fi
