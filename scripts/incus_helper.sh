#!/usr/bin/env bash
set -e

function incus_exec() {
  handle_failure() {
    incus exec "${1}" bash
    echo "Previous command failed. Do you want to abort? [y/n]"
    while true; do
      read -r input
      case "${input}" in
        y | Y)
          exit 1
          ;;
        n | N)
          break
          ;;
        *)
          echo "?"
          ;;
      esac
    done
  }
  set -- incus exec "${@}"
  echo "${@}"
  "${@}" || handle_failure "${3}"
}

function ping_containers() {
  local failure="0"
  local exitcode
  for container in "${@}"; do
    incus exec "${container}" -- ping -c 3 gentoo.org &>/dev/null
    exitcode="${?}"
    if [[ ${exitcode} != 0 ]]; then
      failure="1"
    fi
  done

  echo "${failure}"
}

function start_instance() {
  for instance in "${@}"; do
    if ! incus_info=$(incus info "${instance}"); then
      if [[ ${incus_info} =~ "Instance not found" ]]; then
        echo "Instance ${instance} not found"
        exit 1
      else
        echo "Failed to get info for instance ${instance}"
        exit 2
      fi
    fi
    if [[ ${incus_info} =~ "Status: STOPPED" ]]; then
      start_instances+=("${instance}")
    fi
  done

  if [[ -n ${start_instances[*]} ]]; then
    echo "Starting instances: ${start_instances[*]}"
    incus start "${start_instances[@]}"
    printf "Waiting for networking."

    local success=1
    for ((i = 1; i <= 10; i++)); do
      if [[ $(ping_containers "${start_instances[*]}") == 0 ]]; then
        printf " DONE!\n"
        success=0
        break
      else
        sleep 1
        printf "."
      fi
    done
    if [[ ${success} != 0 ]]; then
      printf " TIMED OUT!\n"
      exit 1
    fi
  fi
}

function update_instance() {
  echo "Updating instance ${1}"
  incus_exec "${1}" -- emerge -uUDj --with-bdeps=y @world
  if incus exec "${1}" -- bash -c "type -P smart-live-rebuild" >/dev/null; then
    incus_exec "${1}" -- emerge -j @smart-live-rebuild
  fi
}

function clean_instance() {
  echo "Cleaning up instance ${1}"
  incus_exec "${1}" -- emerge -c
  incus_exec "${1}" -- eclean distfiles
  incus_exec "${1}" -- eclean packages
  if incus exec "${1}" -- bash -c "type -P eclean-kernel" >/dev/null; then
    incus_exec "${1}" -- eclean-kernel -n 1
  fi
  incus_exec "${1}" -- bash -c "rm -rf /var/tmp/portage/*"
}

function stop_instance() {
  echo "Stopping instances: ${*}"
  incus stop -f "${@}"
}

function test_atom() {
  # TODO: ability to pass env
  # TODO: to modify emerge behavior in test phase

  usage_test() {
    echo "Usage: ${0} test [ --help ] [ --instance instance ] [ --extra-opts option ] [ --no-deps ] [ --no-clean ] [ --no-update ] [ --only-test ] [ atom ] ..."
  }

  OPTS="$(getopt --name="${0}" --options='' --longoptions='help,instance:,extra-opts:,no-deps,no-clean,no-update,only-test' -- "${@}")"
  eval set -- "${OPTS}"
  while true; do
    case "${1}" in
      --help)
        usage_test
        exit 0
        ;;
      --instance)
        instance="${2}"
        shift 2
        ;;
      --extra-opts)
        extraopts="${2}"
        shift 2
        ;;
      --no-deps)
        nodeps="1"
        shift
        ;;
      --no-clean)
        noclean="1"
        shift
        ;;
      --no-update)
        noupdate="1"
        shift
        ;;
      --only-test)
        noclean="1"
        noupdate="1"
        shift
        ;;
      --)
        shift
        break
        ;;
      *)
        usage_test
        exit 2
        ;;
    esac
  done

  for arg in "${@}"; do
    atom+=("${arg}")
  done

  if [[ -z "${instance}" ]]; then
    echo "No instance set"
    usage_test
    exit 2
  fi

  start_instance "${instance}"
  if [[ -z "${noupdate}" ]]; then
    update_instance "${instance}"
  fi
  echo "Testing packages: ${atom[*]}"
  if [[ -z "${nodeps}" ]]; then
    incus_exec "${instance}" -- emerge -oj --with-test-deps "${atom[@]}"
  fi
  incus_exec "${instance}" --env FEATURES="test" -- emerge -1 "${extraopts[@]}" "${atom[@]}"
  if [[ -z "${noclean}" ]]; then
    clean_instance "${instance}"
  fi
  stop_instance "${instance}"
}

function update_instances() {
  if [[ -z "${1}" ]]; then
    echo "Usage: ${0} update  [ instance ] ..."
    exit 2
  fi
  start_instance "${@}"
  for instance in "${@}"; do
    update_instance "${instance}"
    clean_instance "${instance}"
  done
  stop_instance "${@}"
}

if ! type -P incus >/dev/null; then
  echo "This requires incus. Preconfigured preferably"
  exit 1
fi

case "${1}" in
  test)
    shift
    test_atom "${@}"
    ;;
  update)
    shift
    update_instances "${@}"
    ;;
  *)
    echo "Usage: ${0} {test,update} ..."
    exit 2
    ;;
esac
