#!/usr/bin/env bash
set -e

clean=
no_verify=
skip_live=
skip_portage=
skip_sync=
enable_jobs=
jobs_amount=

usage() {
  echo "Usage: $0 [-h|--help] [-c|--clean] [--no-verify] [--skip-live] [--skip-portage] [--skip-sync] [-j|--jobs=JOBS] "
}

OPTS="$(getopt --name="${0}" --options='chj::' --longoptions='help,clean,no-verify,skip-live,skip-portage,skip-sync,jobs::' -- "${@}")"
eval set -- "${OPTS}"
while true; do
  case "${1}" in
    -h | --help)
      usage
      exit 0
      ;;
    -c | --clean)
      clean=1
      shift
      ;;
    --no-verify)
      no_verify=1
      shift
      ;;
    --skip-live)
      skip_live=1
      shift
      ;;
    --skip-portage)
      skip_portage=1
      shift
      ;;
    --skip-sync)
      skip_sync=1
      shift
      ;;
    -j | --jobs)
      enable_jobs=1
      if [[ -n ${2} ]]; then
        if ! [[ "${2}" =~ ^[0-9]+$ ]]; then
          echo "Invalid input for --jobs"
          exit 2
        fi
        jobs_amount="${2}"
      fi
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *)
      usage
      exit 2
      ;;
  esac
done

for arg in "${@}"; do
  case "${arg}" in
    *)
      usage
      exit 2
      ;;
  esac
done

if [[ "${USER}" != "root" ]]; then
  echo "root user required to execute script"
  exit 1
fi

check_dep() {
  if ! "${@}" 1>/dev/null; then
    echo "$1 is not available"
    exit 1
  fi
}

check_dep portageq --version
check_dep pquery --version
check_dep qatom --version
check_dep emaint --version
check_dep emerge --version
check_dep smart-live-rebuild --version
check_dep eclean --version
check_dep eclean-kernel --version

# remove arguments that require user action
declare -a _EMERGE_DEFAULT_OPTS=()
for arg in $(portageq envvar EMERGE_DEFAULT_OPTS); do
  case "${arg}" in
    -a | --ask) ;;
    --autounmask) ;;
    *)
      _EMERGE_DEFAULT_OPTS+=("${arg}")
      ;;
  esac
done

if [[ -n ${enable_jobs} ]]; then
  if [[ -n ${jobs_amount} ]]; then
    _EMERGE_DEFAULT_OPTS+=(--jobs="${jobs_amount}")
  else
    _EMERGE_DEFAULT_OPTS+=(--jobs)
  fi
fi

export EMERGE_DEFAULT_OPTS="${_EMERGE_DEFAULT_OPTS[*]}"
unset _EMERGE_DEFAULT_OPTS

verbose_command() {
  echo ">>> ${*}"
  "${@}"
}

if [[ -z ${skip_sync} ]]; then
  verbose_command emaint sync -A
fi

if [[ -z ${skip_portage} ]]; then
  # Check if portage needs to be updated
  if [[ $(qatom -F "%{PV}" "$(pquery -I sys-apps/portage)") == "9999" ]]; then
    verbose_command smart-live-rebuild -f "sys-apps/portage"
  elif [[ -n $(pquery -u sys-apps/portage) ]]; then
    verbose_command emerge -1u sys-apps/portage
  fi
fi

if [[ -z ${no_verify} ]]; then
  verbose_command emerge --ask --alert --update --changed-use --deep @world
else
  verbose_command emerge --update --changed-use --deep @world
fi

if [[ -z ${skip_live} ]]; then
  export FEATURES="fail-clean"
  verbose_command emerge --keep-going @smart-live-rebuild
fi

if [[ -n ${clean} ]]; then
  # --ask unconditionally
  verbose_command emerge --ask --alert --depclean
  verbose_command eclean packages
  verbose_command eclean distfiles
  verbose_command eclean-kernel
fi
